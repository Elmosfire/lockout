import bpy

from mathutils import Euler
import json

def robot_body(name):
    return bpy.data.objects[f"Body_{name}"]

def set_location(props):
    locs = props["locs"]
    for robot in "ABCDEFGHQ":
        if robot not in locs:
            robot_body(robot).location.x = 1000
            robot_body(robot).location.y = 1000
        else:
            x,y, rot = locs[robot]
            robot_body(robot).location.x = (x-2)*2.77
            robot_body(robot).location.y = (y-2)*2.77
            robot_body(robot).rotation_euler = Euler((0,0,(3-rot)*3.1415926/2), 'XYZ')
    height = props["height"]
    bpy.data.objects["Center"].location.z = bpy.data.objects["CenterB"].location.z + height
    robot_body("Q").location.z = robot_body("A").location.z + height
    
with open(bpy.data.filepath + '\\..\\fname.txt') as file:
    local_file = ''.join(file).strip()
            
with open(bpy.data.filepath + '\\..\\' + local_file) as file:
  animation = json.load(file)
 
INTRO = 100

def update_pos(*_):
    print("changed")
    frame_num = bpy.context.scene.frame_current
    if frame_num > INTRO:
        set_location(animation[frame_num-INTRO])
    else:
        set_location(animation[0])
    
scn = bpy.context.scene 


# assign new duration value
scn.frame_end = len(animation) + INTRO
print(len(animation))

set_location(animation[0])
    
bpy.types.RenderSettings.use_lock_interface = True
bpy.app.handlers.frame_change_pre.append(update_pos)
            
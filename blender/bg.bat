for /L %%A in (0,1,40) do (
    set prefix=out
    IF EXIST %prefix%%%A.mp4 (
        echo File %prefix%%%A.mp4 EXISTS!
    ) ELSE (
        echo %%A > data.txt
        "C:\Program Files\Blender Foundation\Blender 2.93\blender.exe" -b robots.blend --python-expr "findex=%%A" -P Setup.py -o //%prefix%%%A.avi -a
	ffmpeg.exe -i %prefix%%%A.avi -c:v libx265 -x265-params lossless=1 -c:a libfdk_aac -b:a 128k -y %prefix%%%A.mp4
    )
)


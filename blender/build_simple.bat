
IF EXIST ---.mp4 (
    echo ---.mp4 EXISTS!
) ELSE (
    echo ---.json > fname.txt
    "C:\Program Files\Blender Foundation\Blender 2.93\blender.exe" -b robots.blend -P Setup.py -o //---.avi -a
    ffmpeg.exe -i ---.avi -c:v libx265 -x265-params lossless=1 -c:a libfdk_aac -b:a 128k -y ---.mp4
)
del ---.json
del ---.avi


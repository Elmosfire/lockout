#!/bin/bash
NAME=scalingturtle
echo "[main]" > setup_config.ini
#name
echo "name=${PWD##*/}" >> setup_config.ini
#version
revisioncount=`git log --oneline | wc -l| tr -d ' '`
projectversion=`git describe --tags --long || echo 0.0`
cleanversion=${projectversion%%-*}
echo "version=$cleanversion.$revisioncount" >> setup_config.ini
#url
url=https://gitlab.com/$(git remote get-url origin | sed 's/https:\/\/gitlab.com\///' | sed 's/git@gitlab.com://' | sed 's/.git$//')
echo "url=$url" >> setup_config.ini
#licence
echo "license=GPL3.0" >> setup_config.ini
#name
echo "[author]" >> setup_config.ini
echo "name=$(git config --get user.name)" >> setup_config.ini
#email
echo "email=$(git config --get user.email)" >> setup_config.ini
#package
echo "[package]" >> setup_config.ini
echo "package=${PWD##*/}" >> setup_config.ini
git add setup_config.ini

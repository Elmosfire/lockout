from setuptools import setup, find_packages
from configparser import ConfigParser

parser = ConfigParser()
parser.read("setup_config.ini")
confdict = {section: dict(parser.items(section)) for section in parser.sections()}

with open("requirements.txt") as f:
    required = [x for x in f.read().splitlines() if not x.startswith("git+")]
    
print(required)

setup(
    name=confdict["main"]["name"],
    version=confdict["main"]["version"],
    description="no description given",
    url=confdict["main"]["url"],
    author=confdict["author"]["name"],
    author_email=confdict["author"]["email"],
    license=confdict["main"]["license"],
    packages=find_packages(),
    install_requires=required,
    classifiers=[],
)

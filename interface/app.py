

from flask import Flask, render_template
from lockout.state import State

app = Flask(__name__)

@app.route('/')
def hello():
    return render_template('index.html', data=str(State.random("grace").as_js_puzzle()))


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
from distutils.command.build import build
from pathlib import Path
import zipfile
import sys


def scan_animation_data_files(spd, prefix):
    with zipfile.ZipFile(f"lockout_animation_locs_{prefix}_spd_{spd}.zip") as zfile:
        for x in zipfile.Path(zfile).iterdir():
            if x.name.endswith(".json"):
                yield x.name[:-5], x.name


def preproc_blender(spd, prefix):
    with open("blender/build_simple.bat") as file:
        build_simple = "".join(file)
    with zipfile.ZipFile(f"lockout_animation_locs_{prefix}_spd_{spd}.zip") as origfile:
        with zipfile.ZipFile(f"{prefix}_{spd}.zip", mode="x") as zfile:
            zfile.write("blender/Robots.blend", arcname="Robots.blend")
            zfile.write("blender/Setup.py", arcname="Setup.py")
            zfile.write("blender/ffmpeg.exe", arcname="ffmpeg.exe")
            with zfile.open("build.bat", "w") as file:
                for stem, name in scan_animation_data_files(spd, prefix):
                    file.write(build_simple.replace("---", stem).encode("utf-8"))
                file.write("\nDEL Robots.blend\n".encode("utf-8"))
                file.write("DEL Setup.py\n".encode("utf-8"))
                file.write("DEL ffmpeg.exe\n".encode("utf-8"))
                file.write("DEL build.bat\n".encode("utf-8"))
            for stem, name in scan_animation_data_files(spd, prefix):
                origfile.extract(name)
                zfile.write(name, arcname=name)
                Path(name).unlink()


if __name__ == "__main__":
    preproc_blender(10, "generated")

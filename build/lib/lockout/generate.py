from random import sample, shuffle, choice

try:
    from .state import State
except ImportError:
    from state import State
from tqdm import tqdm
from itertools import count
from collections import deque
import logging

logger = logging.getLogger(__name__)


def improve(state: State):
    q = deque()
    q.append(state)
    fitness = 2
    skipped = set()
    best = state
    while len(q):
        nstate = q.pop()
        if len(q) % 100 == 0:
            logger.info(f"fitness: {fitness}; pool={len(q)}")
        if nstate in skipped:
            continue
        skipped.add(nstate)
        if nstate.difficulty() < fitness:
            continue
        if nstate.difficulty() > fitness:
            fitness = nstate.difficulty()
            logger.info(f"fitness: {fitness}; pool={len(q)}")
            best = nstate

        for tokenpos in nstate.get_token_locations():
            x, y = tokenpos
            for xx in range(5):
                if xx != x:
                    q.append(nstate.with_token_moved(x, y, xx, y))
            for yy in range(5):
                if yy != y:
                    q.append(nstate.with_token_moved(x, y, x, yy))
    return best == state, best


def generate(max_=20):
    pool = [(x, y) for x in range(5) for y in range(5)]
    for _ in tqdm(range(max_)):
        r = list(sample(pool, choice([3, 4, 5, 6, 7, 8, 9])))
        logger.info(f"number of bots: {len(r)}")
        shuffle(r)
        state = State.build(*r)
        _, state = improve(state)

        v, sol = state.find_solution(50)

        logger.info(f"solution size: {len(state.get_token_locations())}")
        logger.info(f"solution size: {len(state.get_token_locations())}")

        if not v:
            continue
        if len(sol) > 10:
            logger.info("added state {state}")
            state.store("generated")


if __name__ == "__main__":
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    logger.info("logging")
    generate()

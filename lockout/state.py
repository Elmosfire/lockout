from dataclasses import dataclass
from mmap import ACCESS_WRITE
from typing import Set, Dict, Tuple
from functools import lru_cache
from collections import namedtuple, deque
import json
import logging
import numpy as np
import h5py
from itertools import count
import diskcache as dc
from random import choice
from bidict import bidict

cache = dc.Cache("storage")

logger = logging.getLogger(__name__)

Token = namedtuple("Token", "is_astronaut")


class OutsideFieldError(IndexError):
    pass




@dataclass(frozen=True)
class State:
    tokens: Dict[Tuple[int, int], Token]
    
    # # # # # # # # # # # # # # # # # # #
    # Convertion to different formats   
    # # # # # # # # # # # # # # # # # # #

    @classmethod
    def build(cls, astronout, *tokens):
        return cls(
            {
                (x, y): Token(True)
                if (x, y) == tuple(astronout)
                else Token(False)
                if (x, y) in map(tuple, tokens)
                else None
                for x in range(5)
                for y in range(5)
            }
        )

    def compress(self):
        res = deque()
        for x in range(5):
            for y in range(5):
                token = self.tokens[x, y]
                if token is not None:
                    if token.is_astronaut:
                        res.appendleft([x, y])
                    else:
                        res.append([x, y])
        res = list(res)
        logger.info("stored", res)
        return res

    def to_bytes(self):
        return "".join("".join(str(x) for x in y) for y in self.compress()).encode(
            "utf-8"
        )

    @classmethod
    def from_bytes(self, bytes):
        s = list(map(int, bytes.decode("utf-8")))
        return self.build(*zip(s[::2], s[1::2]))
    
    def annotate_dependant(self, prev):
        try:
            lost_letter = next(v for k, v in prev.items() if self.tokens[k] is None)
        except StopIteration:
            assert False, f"{prev} and {self.tokens} do not differ in exactly one spot"
        res = bidict()
        for k, v in self.tokens.items():
            if v is None:
                continue
            if k in prev:
                res[k] = prev[k]
            else:
                res[k] = lost_letter
        return res
    
    def annotate_free(self):
        letters = list(reversed("abcdefghijklmnopqrstuvw"))
        res = bidict()
        for k, v in sorted(self.tokens.items(), key=lambda s: (s[0][1], s[0][0])):
            if v is None:
                continue
            elif v.is_astronaut:
                res[k] = "x"
            else:
                res[k] = letters.pop()
        return res
    
    def annotate(self, other=None):
        if other is None:
            return self.annotate_free()
        else:
            return self.annotate_dependant(other)

    def as_js_puzzle(self):
        letters = list(reversed("abcde"))
        res = {}
        for (x,y), v in self.tokens.items():
            r = f"{y},{x}"
            if v is None:
                continue
            elif v.is_astronaut:
                res["x"] = r
            else:
                res[letters.pop()] = r
        return json.dumps(res)
    
    # # # # # # # # # # # # # # # # # # #
    # Storage                           
    # # # # # # # # # # # # # # # # # # #


    def register(self, name):
        cache[name.encode("utf-8")] = self.to_bytes()

    def store(self, title):
        past_ushs = {}
        for x in get_all_puzzles():
            past_ushs[x[0]] = x[1].unique_sym()
        sym = self.unique_sym()
        if sym in past_ushs.values():
            loc = next(k for k, v in past_ushs.items() if v == sym)
            logger.warn(f"Puzzle already exists in database as {loc} ({sym})")
            return
        for i in count():
            if f"{title}_{i:08}" not in valid_puzzles():
                break
        logger.info(f"save as {title}_{i:08}")
        self.register(f"{title}_{i:08}")
        return f"{title}_{i:08}"

    @classmethod
    def load(cls, name):
        return cls.from_bytes(cache[name.encode("utf-8")])
    
    @classmethod
    def random(cls, prefix):
        return choice([s for x,s in get_all_puzzles() if x.startswith(prefix)])
    
    # # # # # # # # # # # # # # # # # # #
    # Utilities                        
    # # # # # # # # # # # # # # # # # # #

    def __hash__(self):
        def val(token):
            if token is None:
                return 0
            elif token.is_astronaut:
                return 1
            else:
                return 2

        return sum(3 ** (5 * x + y) * val(v) for (x, y), v in self.tokens.items())

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __repr__(self):
        def disp(token):
            if token is None:
                return "."
            elif token.is_astronaut:
                return "X"
            else:
                return "#"

        return "|".join(
            "".join(disp(self.tokens[x, y]) for x in range(5)) for y in range(5)
        )
        
    # # # # # # # # # # # # # # # # # # #
    # Movement and locations                        
    # # # # # # # # # # # # # # # # # # #

    def size(self):
        return sum(1 for sys in self.tokens.values() if sys is not None)

    def get_token_at_position(self, x, y):
        if not (0 <= x < 5 and 0 <= y < 5):
            raise OutsideFieldError(x, y)
        else:
            return self.tokens[x, y]
        
    def get_token_locations(self):
        return {loc for loc, sys in self.tokens.items() if sys is not None}

    def valid_move(self, x, y, dx, dy):
        token = self.get_token_at_position(x, y)
        assert token is not None, "Cannot move non existing token"
        assert (
            abs(dx) + abs(dy) == 1 and dx * dy == 0
        ), "(dx,dy) must be a vector with size 1 in an orthogonal direction"
        previous_position = x, y
        current_position = x + dx, y + dy
        while True:
            try:
                if self.get_token_at_position(*current_position) is not None:
                    if previous_position == (x, y):
                        return None
                    else:
                        return previous_position
                previous_position = current_position
                current_position = current_position[0] + dx, current_position[1] + dy
            except OutsideFieldError:
                return None
            
    def with_squares_swapped(self, x, y, tx, ty):
        cp = self.tokens.copy()
        cp[tx, ty],cp[x, y]  = cp[x, y], cp[tx,ty]
        return State(cp)
    
    
    def perform_move(self, x, y, dx, dy):
        target = self.valid_move(x, y, dx, dy)
        assert target is not None, "Specified move is invalid"
        return self.with_squares_swapped(x,y, *target)


    def all_valid_moves(self):
        for x, y in self.get_token_locations():
            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                if self.valid_move(x, y, dx, dy) is not None:
                    yield (x, y, dx, dy)

    def next_states(self):
        for x in self.all_valid_moves():
            yield self.perform_move(*x), x
            
    # # # # # # # # # # # # # # # # # # #
    # Solutions                     
    # # # # # # # # # # # # # # # # # # #

    def is_final(self):
        try:
            return self.get_token_at_position(2, 2).is_astronaut
        except (AttributeError, OutsideFieldError):
            return False

    def baseopt_solutions(self, max_depth=1000):
        """
        This function produces at least one symmetry of every optimal solution
        Where optimal means there exists no other solution with a lesser amount of moves
        """
        q = deque()
        q.append((self, ()))
        depth = -1
        skipped = set()
        
        while len(q):
            s, sol = q.popleft()
            if len(sol) > depth:
                depth += 1
                # print("depth", depth, len(q), len(skipped))
                if depth > max_depth:
                    return
            for x, m in s.next_states():
                if hash(x) in skipped:
                    continue
                # print(len(sol), '\n', x)
                nsol = sol + (m,)
                if x.is_final():
                    max_depth = depth
                    yield Solution(self, nsol)
                else:
                    skipped.add(hash(x))
                    q.append((x, nsol))
    
    def single_optimal_solution(self, max_depth=1000):
        """
        Produces a single optimal solution
        """
        try:
            return True, next(self.baseopt_solutions(max_depth))
        except StopIteration:
            return False, None

    def difficulty(self):
        """
        Returns the number of moves of every optimal solution
        """
        sol = self.single_optimal_solution()[1]
        if sol is None:
            return 0
        return sum(1 for _ in sol.states())
    
    def difficulty_nats(self):
        """
        Returns the number of active token switches for the optimal solution
        """
        sol = self.optimal_nats_solution()
        if sol is None:
            return 0
        return sum(1 for _ in sol.annotate_moves_compact())
    
    def optimal_nats_solution(self):
        """
        Return a solution from the optimal sulutions with the least nessesairy active token switching
        """
        return min(self.all_optimal_solutions(), key=lambda s: sum(1 for _ in s.annotate_moves_compact()))
    
    def all_optimal_solutions(self):
        for x in self.baseopt_solutions():
            yield from x.connected()

    # # # # # # # # # # # # # # # # # # #
    # Transformation                     
    # # # # # # # # # # # # # # # # # # #
                

    def rotate(self):
        return State({(4 - y, x): v for (x, y), v in self.tokens.items()})

    def flip(self):
        return State({(y, x): v for (x, y), v in self.tokens.items()})


    # # # # # # # # # # # # # # # # # # #
    # Symmetry                     
    # # # # # # # # # # # # # # # # # # #
    
    def get_hashes(self):
        s = self
        verify = hash(s)
        for _ in range(2):
            s = s.flip()
            for __ in range(4):
                s = s.rotate()
                yield s
        assert hash(s) == verify, f"{s} does not match {self}"

    @lru_cache(maxsize=4096)
    def unique_sym(self):
        return min(map(hash, self.get_hashes()))
    
    # # # # # # # # # # # # # # # # # # #
    # Classification                     
    # # # # # # # # # # # # # # # # # # #

    def related_states(self):
        for tokenpos in self.get_token_locations():
            x, y = tokenpos
            for xx in range(5):
                if xx != x:
                    yield self.with_squares_swapped(x, y, xx, y)
            for yy in range(5):
                if yy != y:
                    yield self.with_squares_swapped(x, y, x, yy)

        for x in range(5):
            for y in range(5):
                if self.tokens[x, y] is None:
                    cp = self.tokens.copy()
                    cp[x, y] = Token(is_astronaut=False)
                    yield State(cp)
                elif self.tokens[x, y].is_astronaut is False:
                    cp = self.tokens.copy()
                    cp[x, y] = None
                    yield State(cp)


    
@dataclass(frozen=True)
class Solution:
    state: State
    moves: Tuple[Tuple[int,int,int,int]]
    def states(self):
        s = self.state
        yield s
        for x in self.moves:
            s = s.perform_move(*x)
            yield s

    def verify(self,):
        s = self.state
        for x in self.moves:
            try:
                s = s.perform_move(*x)
            except AssertionError:
                return False
        return s.is_final()
    
    def connected(self):
        Q = deque()
        final = set()
        skip = set()
        Q.append(self)
        while Q:
            x = Q.popleft()
            if x in final or x in skip or x in Q:
                continue
            if not x.verify():
                skip.add(x)
                continue
            final.add(x)
            for i in range(len(x.moves) - 1):
                z = list(x.moves)
                z[i], z[i+1] = z[i+1], z[i]
                Q.append(Solution(self.state, tuple(z)))
        return final
    
    def annotate_states(self):
        prev = None
        for current in self.states():
            prev = current.annotate(prev)
            yield prev
            
    def annotate_moves(self):
        names = {(-1,0):'L', (1,0):'R', (0,-1):"U", (0,1):"D"}
        for (x,y,dx,dy), annotate in zip(self.moves, self.annotate_states()):
            yield annotate[(x,y)], names[dx,dy]
    
    def annotate_moves_compact(self):
        currset = []
        currlead = ""
        for x,y in self.annotate_moves():
            if x == currlead:
                currset.append(y)
            else:
                if currlead:
                    yield currlead, currset
                currset = [y]
                currlead = x
        yield currlead, currset
        

def get_all_puzzles():
    for x in valid_puzzles():
        yield x, State.load(x)


def valid_puzzles():
    for x in cache.iterkeys():
        yield x.decode("utf-8")
        

def delete_puzzle(name):
    print("deleted", name)
    cache.delete(name.encode("utf-8"))
    

def extract(lines):
    res = {}
    if isinstance(lines, str):
        lines = lines.split("\n")
    for x in range(5):
        for y in range(5):
            if lines[y][x] in "abcde#":
                res[x, y] = Token(False)
            elif lines[y][x] in "xX":
                res[x, y] = Token(True)
            elif lines[y][x] == ".":
                res[x, y] = None
            else:
                assert False
    return State(res)

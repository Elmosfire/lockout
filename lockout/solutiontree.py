try:
    from state import State, get_all_puzzles, delete_puzzle
    from grace import full_sys, StateSys
    from animate_simple import build
    from animationdata import animate_all
    from preproq_blender import preproc_blender
except ImportError:
    from .state import State, get_all_puzzles, delete_puzzle
    from .grace import full_sys, StateSys
    from .animate_simple import build
    from .animationdata import animate_all
    from .preproq_blender import preproc_blender

from itertools import chain
import os

from re import X
import networkx as nx

from lxml.html import builder as H
from lxml import html
from tqdm import tqdm

def get_html_widget(state, name):
    return html.tostring(H.TABLE(
        H.TR(H.TD(name, colspan="5")),
        H.TR(H.TD(str(state.size()), colspan="3"), H.TD(str(state.difficulty()), colspan="2")),
        *[
            H.TR(
                *[
                    H.TD(
                        " ",
                        bgcolor=(
                            "white"
                            if state.tokens[x, y] is None
                            else "red"
                            if state.tokens[x, y].is_astronaut
                            else "black"
                        ),
                        width="20%"
                    )
                    for x in range(5)
                ]
            )
            for y in range(5)
        ],
    )).decode("utf-8")


def full_database_sysgraph(sz):
    names = {}
    states = set()

    for x in full_sys():
        if x.size() == sz:
            states.add(x)
        names[x.unique_sym()] = f"({x.unique_sym()})"

    for x in get_all_puzzles():
        names[x[1].unique_sym()] = x[0]
        if x[1].size() == sz:
            states.add(x[1])

    return SysGraph(states, names)


class SysGraph:
    def __init__(self, states, names):
        self.graph = nx.DiGraph()
        self.names = names
        self.names2 = {}
        for x, y in tqdm(get_all_puzzles(), desc="load db"):
            self.names2[y.unique_sym()] = x
        for x in tqdm(states, desc="building graph"):
            self.register_state_sol(x)

    def register(self, state):
        key = state.unique_sym()
        if key not in self.graph.nodes:
            label = get_html_widget(state, self.names.get(key, f"{key}"))
            self.graph.add_node(key, label=f"<{label}>", shape="box")
            
    def register_series(self, lst):
        lst = iter(lst)
        prev = next(lst)
        self.register(prev)
        prev = prev.unique_sym()
        for x in lst:
            curr = x.unique_sym()
            self.register(x)
            if not self.graph.has_edge(prev, curr):
                self.graph.add_edge(prev, curr)
            prev = curr

    def register_state_sol(self, state: State):
        try:
            for y in state.sol_gen_all():
                self.register_series(y)
        except TypeError:
            raise
            pass

    def find_leafs(self):
        for x in tqdm(self.graph.nodes, desc="scan"):
            if not self.is_leaf(x):
                continue
            if not all(self.is_leaf(y) for y in self.get_hand(x)):
                continue
            if any(y in self.names2 for y in self.get_hand(x)):
                continue
            
            yield x


    def store_leafs(self, name, limit_=8):
        done = StateSys().done
        for x in self.find_leafs():
            state = done.get(x)
            if state.difficulty() >= limit_:
                self.names2[x] = state.store(name)
                label = get_html_widget(state, self.names2[x])
                self.graph.nodes[x]["label"] = f"<{label}>"
                print("stored", self.names2[x])
                
    def get_hand(self, leaf, levels=3):
        row = [leaf]
        for _ in range(levels):
            row = list(chain(*(self.graph.successors(x) for x in row)))
        for _ in range(levels):
            row = list(chain(*(self.graph.predecessors(x) for x in row)))
        assert (not row) or leaf in row
        yield from row

    def is_leaf(self, leaf):
        return all(False for _ in self.graph.predecessors(leaf))

    def save(self, fname):
        nx.drawing.nx_pydot.write_dot(self.graph, fname)

def export_by_sz(sz):
    s = full_database_sysgraph(sz)
    s.store_leafs(f"grace_{sz}")
    s.save(f"grace_{sz}.gv")
    print(f"dot grace_{sz}.gv -o grace_{sz}.svg -Tsvg")
    os.system(f"dot grace_{sz}.gv -o grace_{sz}.svg -Tsvg")

if __name__ == "__main__":
    for x, y in tqdm(get_all_puzzles(), desc="clear db"):
        if x.startswith("grace"):
            delete_puzzle(x)
    
    export_by_sz(4)
    export_by_sz(5)
    export_by_sz(6)
    export_by_sz(7)
    
    animate_all(10,"grace")
    build(10,"grace")
    preproc_blender(10,"grace")

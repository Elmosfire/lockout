from functools import lru_cache
import numpy as np
import json
from skopt.optimizer import forest_minimize
import logging
try:
    from .state import get_all_puzzles
except ImportError:
    from state import get_all_puzzles
from pathlib import Path
import zipfile
from itertools import chain
from random import shuffle, random

logger = logging.getLogger(__name__)


def get_sys(s):
    return {k: v for k, v in s.tokens.items() if v is not None}


def get_sol_sys(s):
    for solution in s.all_optimal_solutions():
        yield map(get_sys, solution.states())


def mark_tokens_first(d):
    letters = list(reversed("ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
    res = {}
    for k, v in d.items():
        if v.is_astronaut:
            res[k] = "Q"
        else:
            res[k] = letters.pop()
    return res


def mark_tokens_other(d, prev):
    lost_letter = next(v for k, v in prev.items() if k not in d)
    res = {}
    for k, v in d.items():
        if k in prev:
            res[k] = prev[k]
        else:
            res[k] = lost_letter
    return res


def mark(s):
    prev = None
    for d in s:
        if prev is None:
            prev = mark_tokens_first(d)
        else:
            prev = mark_tokens_other(d, prev)
        yield {v: k for k, v in prev.items()}


def add_directions(s):
    prev = None
    for d in s:
        if prev is None:
            prev = {k: (x, y, 0 if y > 2 else 2) for k, (x, y) in d.items()}
        else:
            target = next(k for k in prev if tuple(prev[k][:2]) != d[k])
            sx, sy, _ = prev[target]
            ex, ey = d[target]
            axis = 1 if (ex - sx) != 0 else 0
            neg = 2 if ex + ey > sx + sy else 0
            dir = axis + neg
            yield prev.copy()
            prev[target] = (sx, sy, dir)
            yield prev.copy()
            prev = {k: (x, y, prev[k][2]) for k, (x, y) in d.items()}
    yield prev.copy()


def pairs(iterator):
    iterator = iter(iterator)
    prev = next(iterator)
    for x in iterator:
        yield prev, x
        prev = x


def animate_pair(start, end_, spd=30):
    try:
        target = next(k for k in start if start[k] != end_[k])
    except StopIteration:
        #logger.warn(f"cannot animate between {start} and {end_}")
        return
    sx, sy, sd = start[target]
    ex, ey, ed = end_[target]
    core = start.copy()

    yield core.copy()
    if sd != ed:
        if sd == 0 and ed == 3:
            sd = 4
        if sd == 3 and ed == 0:
            ed = 4
        lenght = abs(sd - ed)
        for t in np.linspace(sd, ed, spd * lenght):
            core[target] = (sx, sy, t)
            yield core.copy()

    if (sx, sy) != (ex, ey):
        lenght = abs(ex - sx) + abs(ey - sy)
        for t in (
            np.array(list(accellerate_stable(spd * lenght, lenght, 0.2, 0.4))) / lenght
        ):
            core[target] = (sx + (ex - sx) * t, sy + (ey - sy) * t, ed)
            yield core.copy()
        yield core.copy()


def animate_mark(s, spd):
    for x, y in pairs(add_directions(mark(s))):
        yield from animate_pair(x, y, spd)


def accellerate(a, max_x, bounce, fric):
    x = 0
    v = 0
    while True:
        v += a
        if v < 0:
            v += 2 * fric
        x += v
        if x > max_x:
            x = max_x
            v *= -bounce
            if abs(v) < a * 4:
                break
        yield x
    yield max_x


@lru_cache(maxsize=8)
def accellerate_stable(t, max_x, bounce, fricfrac):
    def resfunc(a):
        a, *_ = a
        return (sum(1 for _ in accellerate(a, max_x, bounce, fricfrac * a)) - t) ** 2

    opt = forest_minimize(resfunc, [(0.0, 1.0)])
    a, *_ = opt.x
    res = list(accellerate(a, max_x, bounce, fricfrac * a))
    logger.info(f"optimimal acceleratiron for time {t} and distance {max_x} is {a}")
    logger.info(f"real time {len(res)}, target_time {t}")
    return res


def full_animation(s, spd=30):

    for x in animate_mark(s, spd):
        yield {"locs": x, "height": 0}
    for y in np.linspace(0, 4, 150):
        yield {"locs": x, "height": -y}


def animate_all(spd=30, prefix=""):
    try:
        Path(f"lockout_animation_locs_{prefix}_spd_{spd}.zip").unlink()
    except FileNotFoundError:
        pass
    with zipfile.ZipFile(
        f"lockout_animation_locs_{prefix}_spd_{spd}.zip", "x"
    ) as zfile:
        for x, s in get_all_puzzles():
            if x.startswith(prefix):
                logger.info("writing file:", x)
                data = []
                for sol in get_sol_sys(s):
                    data.append(list(full_animation(sol, spd)))
                zfile.writestr(f"{x}.json", json.dumps(data))


def anotate_custom_levels():
    res = []
    for x, s in sorted(get_all_puzzles(), key=lambda x:(x[0].startswith("grace")*random(), x)):
        if s.size() <= 6:
            res.append({"name": x, "data":{v.lower().replace("q", "x"):','.join(map(str, k)) for k,v in mark_tokens_first(get_sys(s)).items()}})
    with open("interface/rawdata.json", "w") as file:
        json.dump(res, file)
                       
            


if __name__ == "__main__":
    anotate_custom_levels()
    #animate_all(10, "grace")
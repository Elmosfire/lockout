from importlib.metadata import entry_points
import drawSvg as draw
from pathlib import Path
from startortoise import build_video_cairo
import json
import zipfile


def scan_animation_data_files(spd, prefix):
    with zipfile.ZipFile(f"lockout_animation_locs_{prefix}_spd_{spd}.zip") as zfile:
        for x in zipfile.Path(zfile).iterdir():
            if x.name.endswith(".json"):
                yield x.name, json.loads(x.read_text())


def draw_state_2(state_dict):
    d = draw.Drawing(500, 300, displayInline=False)
    colors = dict(
        Q="red",
        A="orange",
        B="green",
        C="purple",
        D="yellow",
        E="cyan",
        F="Blue",
        G="pink",
        H="grey",
        I="white",
    )
    d.append(draw.Rectangle(2 * 40 + 150, 2 * 40 + 50, 40, 40, fill="red"))

    for s in range(1, 5):
        d.append(draw.Line(150, s * 40 + 50, 350, s * 40 + 50, stroke="black"))
        d.append(draw.Line(s * 40 + 150, 50, s * 40 + 150, 250, stroke="black"))
    for name, (xx, yy) in state_dict.items():
        x = yy
        y = 4 - xx
        d.append(
            draw.Circle(
                x * 40 + 170, y * 40 + 70, 20, stroke="black", fill=colors[name]
            )
        )
        d.append(
            draw.Text(
                name,
                20,
                x * 40 + 170,
                y * 40 + 70,
                center="True",
                valign="middle",
                vstroke="black",
            )
        )

    return d.asSvg().encode("utf-8")


def rep(iterator, rep=1):
    for x in iterator:
        for y in range(rep):
            yield x


def clean_anim_data(data):
    prev = None
    for x in data:
        locs = x["locs"]
        res = {}
        difference = 0
        for y, r in locs.items():
            res[y] = (r[0], r[1])
            if prev is not None:
                difference += abs(r[0] - prev[y][0]) + abs(r[1] - prev[y][1])
        if difference > 0.1**8:
            yield res
        prev = res


def build(spd, prefix):
    for name, data_full in scan_animation_data_files(spd, prefix):
        data = data_full[0]
        x = Path("simple_animations") / f"{prefix}_{spd}" / name
        x.parent.mkdir(exist_ok=True, parents=True)
        target = x.with_suffix(".mp4")
        targetsvg = x.with_suffix(".svg")

        if targetsvg.exists():
            continue
            target.unlink()
        q = list(clean_anim_data(data))

        q = [q[0]] * 30 + q + [q[-1]] * 30

        build_video_cairo(map(draw_state_2, q), "moon.jpg", str(target), 30)
        with targetsvg.open("w") as file:
            file.write(draw_state_2(q[0]).decode("utf-8"))


if __name__ == "__main__":
    build(10, "grace")
    build(10, "official")

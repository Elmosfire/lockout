try:
    from state import State, get_all_puzzles
except ImportError:
    from .state import State, get_all_puzzles

from sys import get_asyncgen_hooks
from unicodedata import name
import zipfile
from pathlib import Path
import json
from random import sample
from itertools import islice
import logging
from black import Cache
import diskcache as dc
import heapq
from scipy.fftpack import ss_diff
from tqdm import tqdm


logger = logging.getLogger(__name__)


FITNESS_CACHE = dc.Cache(f"Grace/fitness")


def sanitise_key(key):
    if isinstance(key, bytes):
        return key
    else:
        return str(key).encode("utf-8")


class StateStore:
    def __init__(self, name):
        self.cache = dc.Cache(f"{name}")

    def __contains__(self, state):
        return sanitise_key(state.unique_sym()) in self.cache.iterkeys()

    def add(self, state):
        state_as_bytes = state.to_bytes()
        state_key = sanitise_key(state.unique_sym())
        self.cache[state_key] = state_as_bytes

    def remove(self, state):
        state_key = sanitise_key(state.unique_sym())
        self.cache.delete(state_key)

    def get(self, key):
        return State.from_bytes(self.cache[sanitise_key(key)])

    def __iter__(self):
        for x in self.cache.iterkeys():
            yield State.from_bytes(self.cache[x])

    def fastfilter(self, other):
        for x in tqdm(other.cache.iterkeys(), desc="cleaning"):
            if x in self.cache:
                self.cache.delete(x)

    def get_fitness_struct_iterator(self):
        for x in tqdm(self.cache.iterkeys(), desc="fitness"):
            yield (self._fitness(x), x)

    def _fitness(self, state_id: bytes):
        if state_id not in FITNESS_CACHE:
            state = self.get(state_id)
            FITNESS_CACHE[state_id] = (
                (20-state.difficulty()) + 100 * (20 - state.difficulty_nats()) + 10000 * len(state.get_token_locations())
            ).to_bytes(16, "big")
        return int.from_bytes(FITNESS_CACHE[state_id], "big")

    def fitness(self, state):
        key = sanitise_key(state.unique_sym())
        return self._fitness(key), key

    def build_heap(self):
        heap = list(self.get_fitness_struct_iterator())
        heapq.heapify(heap)
        return heap


class StateSys:
    def __init__(self, name="Grace"):
        self.done = StateStore(f"{name}/done")
        self.curr = StateStore(f"{name}/curr")

        self.curr.fastfilter(self.done)

        self.heap = self.curr.build_heap()

    def add_system(self, state: State):
        if state not in self.done and state.difficulty() > 0 and state not in self.curr:
            self.curr.add(state)
            heapq.heappush(self.heap, self.curr.fitness(state))

    def process_sys(self, state: State):
        if state in self.done:
            return
        if state.difficulty() == 0:
            return
        if len(state.get_token_locations()) > 7:
            return
        self.add_system(state)
        for newstate in tqdm(state.related_states(), desc="states"):
            self.add_system(newstate)

        self.done.add(state)

    def seed_database_objects(self):
        for v, x in get_all_puzzles():
            logger.info(f"logging {v}")
            self.process_sys(x)

    def full_sys(self):
        keys = self.sys.keys()
        for x in keys:
            for y in self.sys[x]:
                s = State.build(*y)
                yield s

    def best(self, n):
        try:
            return self.curr.get(heapq.heappop(self.heap)[1])
        except StopIteration:
            return self.random_state(n)

    def improve_single_step(self, n):
        best = self.best(n)
        logger.info(
            f"best {best}; difficulty: {best.difficulty()}|{best.difficulty_nats()} size:{len(best.get_token_locations())}"
        )
        self.process_sys(best)

    @staticmethod
    def random_state(n):
        pool = [(x, y) for x in range(5) for y in range(5)]

        r = list(sample(pool, n))
        return State.build(*r)


def generate(n):
    q = StateSys()
    q.seed_database_objects()
    for _ in range(1000):
        q.improve_single_step(n)


def export(minimal_value=10):
    ss = StateSys()
    q = ss.done.build_heap()
    while q:
        _, key = heapq.heappop(q)
        x = ss.done.get(key)
        difficulty = x.difficulty()
        n = len(x.get_token_locations())
        if difficulty - 2 * n + 12 >= minimal_value:

            x.store(f"grace_{n}_{difficulty}")


def full_sys():
    yield from StateSys().done


if __name__ == "__main__":
    logging.basicConfig()
    logger.setLevel(logging.INFO)
    logger.info("logging")

    generate(6)
